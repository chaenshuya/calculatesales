package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LIST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 支店別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// 正規表現
	private static final String SALES_FILE_EXPRESSION_RULE = "^[0-9]{8}.rcd$";
	private static final String NUMBERS_ONLY_EXPRESSION_RULE = "^[0-9]*$";
	private static final String BRANCH_CODE_EXPRESSION_RULE = "^[0-9]{3}$";
	private static final String COMMODITY_CODE_EXPRESSION_RULE ="^[A-Za-z0-9]{8}$";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String BRANCH_NAMES = "支店定義ファイル";
	private static final String COMMODITY_NAMES = "商品定義ファイル";
	private static final String INVALID_FORMAT = "のフォーマットが不正です";
	private static final String FILE_INVALID_SERIAL_NUMBER = "売上ファイル名が連番になっていません";
	private static final String MORE_THAN_TEN_DIGITS = "合計金額が10桁を超えました";
	private static final String BRANCH_CODE_NOT_EXIST = "の支店コードが不正です";
	private static final String COMMODITY_CODE_NOT_EXIST = "の商品コードが不正です";
	private static final String FILE_NOT_EXIST = "が存在しません";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();

		Map<String, String> commodityNames = new HashMap<>();

		Map<String, Long> commoditySales = new HashMap<>();



		// 支店定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, BRANCH_CODE_EXPRESSION_RULE, BRANCH_NAMES)) {
			return;
		}

		if(!readFile(args[0], FILE_NAME_COMMODITY_LIST, commodityNames, commoditySales, COMMODITY_CODE_EXPRESSION_RULE, COMMODITY_NAMES)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		for(int i = 0; i < files.length; i++) {
			if(files[i].isFile() && files[i].getName().matches(SALES_FILE_EXPRESSION_RULE)){
				rcdFiles.add(files[i]);
			}
		}
		Collections.sort(rcdFiles);


		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if((latter - former) != 1) {
				System.out.println(FILE_INVALID_SERIAL_NUMBER);
				return;
			}
		}
		BufferedReader br = null;

		for(int j = 0; j < rcdFiles.size(); j++) {
			try{
				List<String> fileSale = new ArrayList<>();
				File file = rcdFiles.get(j);
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);
				String line;
				while((line = br.readLine()) != null) {
					fileSale.add(line);
				}
				if(fileSale.size() != 3) {
					System.out.println(rcdFiles.get(j).getName() + INVALID_FORMAT);
					return;
				}
				if(!branchNames.containsKey(fileSale.get(0))) {
					System.out.println(rcdFiles.get(j).getName() + BRANCH_CODE_NOT_EXIST);
					return;
				}

				if(!commodityNames.containsKey(fileSale.get(1))) {
					System.out.println(rcdFiles.get(j).getName() + COMMODITY_CODE_NOT_EXIST);
					return;
				}

				if(!fileSale.get(2).matches(NUMBERS_ONLY_EXPRESSION_RULE)) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				long longSale = Long.parseLong(fileSale.get(2));
				long branchSaleAmount = branchSales.get(fileSale.get(0)) + longSale;
				long commoditySaleAmount = commoditySales.get(fileSale.get(1)) + longSale;


				if(branchSaleAmount >= 10000000000L || commoditySaleAmount >= 10000000000L ){
					System.out.println(MORE_THAN_TEN_DIGITS);
					return;
				}
				branchSales.put(fileSale.get(0),branchSaleAmount);
				commoditySales.put(fileSale.get(1), commoditySaleAmount);

			} catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}
	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales, String expressionRule, String namesForError) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);

			if(!file.exists()) {
				System.out.println(namesForError + FILE_NOT_EXIST);
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");
				if(items.length != 2 || (!items[0].matches(expressionRule))){
					System.out.println(namesForError + INVALID_FORMAT);
					return false;
				}
				names.put(items[0], items[1]);
				sales.put(items[0], 0L);
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
		// ファイルを開いている場合
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> names, Map<String, Long> sales) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)

		BufferedWriter bw = null;

		try{
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			for(String key : names.keySet()){ // すべてのkeyを取得し、その数だけ以下の処理をせよ
				bw.write(key + "," + names.get(key) + "," + sales.get(key));
				bw.newLine();
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
